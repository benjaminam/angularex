// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAMsqZhSnrqL7z-m3RS3wCzxsDRS6fOQk4",
    authDomain: "hello-4488b.firebaseapp.com",
    databaseURL: "https://hello-4488b.firebaseio.com",
    projectId: "hello-4488b",
    storageBucket: "hello-4488b.appspot.com",
    messagingSenderId: "278242183689",
    appId: "1:278242183689:web:949200f34eef2d9b6286d8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
