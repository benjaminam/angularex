import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Posts } from '../interfaces/posts';
import { PostsService } from './../posts.service';


@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts;
  postsData$:Observable<Posts>;
  hasError:Boolean = false;
  errorMessage:string;

  constructor(private PostsService:PostsService) { }

  ngOnInit(): void {
    this.postsData$ = this.PostsService.getPosts();
    this.postsData$.subscribe(
      data => {
        this.posts = data;
      },
      error => {
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message;
      }
      )
  }

}
