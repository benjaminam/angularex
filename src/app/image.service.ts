import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
public images:string[]=[];
private path:string='https://firebasestorage.googleapis.com/v0/b/hello-4488b.appspot.com/o/';
  constructor() {     
    this.images[0]=this.path + 'biz.JPG' + 'alt=media&token=81f9cde4-ea24-461e-8566-cd9163c1ece4';
    this.images[1]=this.path + 'entermnt.JPG' + 'alt=media&token=d27af794-ed76-4cf1-b615-383272f20c94';
    this.images[2]=this.path + 'politics-icon.JPG' + 'alt=media&token=b038ed2e-b2a0-4dc3-a804-a0247ef6216d';
    this.images[3]=this.path + 'sport.JPG' + 'alt=media&token=9861f741-b9b6-4315-8425-56b58db3a306';
    this.images[4]=this.path + 'tech.JPG' + 'alt=media&token=5507fcce-dc7a-474c-9a1d-1ddb33536801';
  }
}
